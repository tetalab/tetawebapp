\echo ******************************
\echo * Dropping database tetawebapp
\echo ******************************

\c postgres;
drop database tetawebapp;

\echo **************************
\echo * Dropping role tetawebapp
\echo **************************
drop role tetawebapp;

\echo ***************************************************
\echo * Creating role tetawebapp with password tetawebapp
\echo ***************************************************
create role tetawebapp with LOGIN ENCRYPTED PASSWORD 'tetawebapp';

\echo ******************************
\echo * Creating database tetawebapp
\echo ******************************
create database tetawebapp;

\echo *******************************************
\echo * Giving tetawebapp ownership to tetawebapp
\echo *******************************************
alter database tetawebapp owner to tetawebapp;

\echo *********************************
\echo * Creating tetawebapp_users table
\echo *********************************

\c tetawebapp;
CREATE TABLE tetawebapp_users (
  id serial primary key,
  mail text not NULL,
  password text not NULL,
  name text not NULL
);

\echo *************************************************
\echo * Giving tetawebapp_users ownership to tetawebapp
\echo *************************************************
alter table tetawebapp_users owner to tetawebapp;

\echo *********************************************************************
\echo * Inserting user demo identified by password demo to tetawebapp_users
\echo *********************************************************************
insert into tetawebapp_users (mail, password, name) values ('demo', '$2b$12$yjv4QMctGJFj2HmmbF6u5uDq9ATIl/Y9Z96MbaqRrcG6AE0CGHKSS', 'demo');
