var red = "#FF0000";
var green = "#00FF00";
var light_red = "#FCD5DC";
var light_green = "#D5FCD8";
var base_bg = "#FFFFFF";
var base_border = "#888888";
var coloured_bg = "#FF5D00";
var clear_bg = "#E5E5E5";
var text_color = "#555555";

/* **************************************************************************************
 * GLOBAL
 * **************************************************************************************/

// Cookies
function setcookie(cname, cvalue, exdays) {
    // Set cookie
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getcookie(cname) {
  // Get cookie by name
  var value = "; " + document.cookie;
  var parts = value.split("; " + cname + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}

// Eye candies
function valid_input(obj) {
  // Valid input makes obj background to glow green for 2 seconds
  // If obj borders were red, they get they normal color back
  obj.style.backgroundColor = light_green;
  obj.style.borderColor = base_border;
  setTimeout( function() {
    obj.style.backgroundColor = base_bg;
    }
  , 2000);
}

function invalid_input(obj) {
  // Invalid input makes obj borders and background to glow red for 2 seconds
  // Border color will stay red until a valid input is sent
  obj.style.backgroundColor = light_red;
  obj.style.borderColor = red;
  setTimeout( function() {
    obj.style.backgroundColor = base_bg;
    }
  , 2000);
}

function valid_upload(obj) {
  // Valid input makes obj background to glow green for 2 seconds
  // If obj borders were red, they get they normal color back
  obj.style.backgroundColor = green;
  obj.style.borderColor = text_color;
  obj.style.borderStyle = 'solid';
  setTimeout( function() {
    obj.style.backgroundColor = clear_bg;
    obj.style.borderStyle = 'none';
    }
  , 2000);
}

function invalid_upload(obj) {
  // Invalid input makes obj borders and background to glow red for 2 seconds
  // Border color will stay red until a valid input is sent
  obj.style.backgroundColor = red;
  obj.style.borderColor = text_color;
  obj.style.borderStyle = 'solid';
  setTimeout( function() {
    obj.style.borderStyle = 'solid';
    obj.style.backgroundColor = clear_bg;
    obj.style.borderColor = red;
    }
  , 2000);
}

function lit(obj) {
  // Lit bacground and border on obj (use by input type=file)
  obj.style.backgroundColor = coloured_bg;
  obj.style.borderColor = text_color;
  obj.style.borderStyle = 'solid';
}

function unlit(obj) {
  // Unlit bacground and border on obj (use by input type=file)
  obj.style.backgroundColor = clear_bg;
  obj.style.borderColor = clear_bg;
  obj.style.borderStyle = 'none';
}


function verify_login() {
  // Verify login inputs
  login = document.getElementById('login');
  password = document.getElementById('password');
  if (login.value.length > 0) {
    valid_input(login);
    if (password.value.length > 0) {
      valid_input(password);
      return true;
    }
    invalid_input(password);
    return false;
  }
  invalid_input(login);
  return false;
}

function logout() {
  // Logout user
  setcookie('token', '', 30);
  document.location = '/';
}

/* **************************************************************************************
 * AJAX
 * **************************************************************************************/

function get_html_from_ajax(obj, url) {
  // Get HTML content from AJAX request from url argument
  // HTML content is then put as innerHTML to obj
  var xhttp = new XMLHttpRequest();
  xhttp.onerror = function(){
    obj.innerHTML = "Error while getting content (1)";
  };

  xhttp.onload = function(){
    if (xhttp.status != 200) {
      obj.innerHTML = "Error while getting content (2)";
      } 
  };

  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var response = xhttp.responseText;
      obj.innerHTML = response;
    }
  };
  xhttp.open('POST', url, true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send();
}

function set_value_from_ajax(obj, url, err_code) {
  // Send value from obj.value via AJAX request to url argument
  // obj.value is passed to URL in a REST sheme like <URL>/<VALUE>
  // If err_code response is received, then a server side
  // error has occured and input is invalidated.
  url = url + '/' + obj.value;
  var xhttp = new XMLHttpRequest();
  xhttp.onerror = function(){
    invalid_input(obj);
  };

  xhttp.onload = function(){
    if (xhttp.status != 200) {
      invalid_input(obj);
      } 
  };

  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var response = xhttp.responseText;
      if (response == err_code) {
        invalid_input(obj);
        return;
      }
      valid_input(obj);
      return;
    }
  };
  xhttp.open('POST', url, true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send();
}

function get_value_from_ajax(obj, url, err_code) {
  // Get value from AJAX request
  // The returned value is then set to obj.value.
  // If err_code response is received, then a server side
  // error has occured and input is invalidated
  var xhttp = new XMLHttpRequest();
  xhttp.onerror = function(){
    invalid_input(obj);
  };

  xhttp.onload = function(){
    if (xhttp.status != 200) {
      invalid_input(obj);
      }
  };

  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var response = xhttp.responseText;
      if (response == err_code) {
        invalid_input(obj);
        return;
      }
      obj.value = response;
      valid_input(obj);
      return;
    }
  };
  xhttp.open('POST', url, true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send();
}

function upload_file_from_ajax(obj, url, err_code) {
  // Upload files get from <obj> to the specified <url>
  // if <errcode> is returned input is invalidated
  var files = obj.files;
  var icon_id = obj.id.substring(obj.id.lastIndexOf("_") + 1);
  var icon_obj = document.getElementById("upload_icon_" + icon_id)
  var xhttp = new XMLHttpRequest();
  xhttp.onerror = function(){
    invalid_upload(icon_obj);
  };

  xhttp.onload = function(){
    if (xhttp.status != 200) {
      invalid_upload(icon_obj);
      } 
  };

  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var response = xhttp.responseText;
      if (response == err_code) {
        invalid_upload(icon_obj);
        return;
      }
      valid_upload(icon_obj);
      return;
    }
  };
  
  xhttp.open('POST', url, true);
  var formData = new FormData();
  for (var i=0; i < files.length; i++){
    formData.append("files", files[i], files[i].name);
  }
  xhttp.send(formData);
}
